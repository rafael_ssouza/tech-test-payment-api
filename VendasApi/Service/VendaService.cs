﻿using VendasApi.Data;
using VendasApi.DTO;
using VendasApi.Model;
using VendasApi.Repositories;

namespace VendasApi.Services
{
    public class VendaService
    {
        private readonly VendaRepository _vendaRepository;
        private readonly VendedorRepository _vendedorRepository;

        public VendaService(VendaRepository vendaRepository, VendedorRepository vendedorRepository)
        {
            _vendaRepository = vendaRepository;
            _vendedorRepository = vendedorRepository;
        }

        public Venda RegistrarVenda(VendaDTO vendaDTO)
        {
            var vendedor = _vendedorRepository.GetById(vendaDTO.Vendedor.Id);
            if (vendedor == null) throw new ArgumentException("Vendedor não encontrado");

            var venda = new Venda
            {
                Vendedor = vendedor,
                Itens = vendaDTO.Itens.Select(i => new Item { Nome = i.Nome, Preco = i.Preco }).ToList(),
                Data = DateTime.Now,
                Status = "Aguardando pagamento"
            };

            return _vendaRepository.Add(venda);
        }

        public Venda BuscarVenda(long id)
        {
            return _vendaRepository.GetById(id);
        }

        public Venda AtualizarStatus(long id, string status)
        {
            var venda = _vendaRepository.GetById(id);
            if (venda == null) throw new ArgumentException("Venda não encontrada");

            if (ValidarTransicao(venda.Status, status))
            {
                venda.Status = status;
                _vendaRepository.Update(venda);
                return venda;
            }
            else
            {
                throw new ArgumentException("Transição de status inválida");
            }
        }

        private bool ValidarTransicao(string statusAtual, string novoStatus)
        {
            var transicoesValidas = new Dictionary<string, List<string>>
            {
                { "Aguardando pagamento", new List<string> { "Pagamento aprovado", "Cancelada" } },
                { "Pagamento aprovado", new List<string> { "Enviado para transportadora", "Cancelada" } },
                { "Enviado para transportadora", new List<string> { "Entregue" } }
            };

            return transicoesValidas.ContainsKey(statusAtual) && transicoesValidas[statusAtual].Contains(novoStatus);
        }
    }
}
