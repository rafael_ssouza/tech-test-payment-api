﻿namespace VendasApi.DTO
{
    public class VendaDTO
    {
        public long Id { get; set; }
        public VendedorDTO Vendedor { get; set; }
        public List<ItemDTO> Itens { get; set; }
        public DateTime Data { get; set; }
        public string Status { get; set; }
    }
}
