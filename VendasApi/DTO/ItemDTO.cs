﻿namespace VendasApi.DTO
{
    public class ItemDTO
    {
        public long Id { get; set; }
        public required string Nome { get; set; }
        public double Preco { get; set; }
    }
}
