﻿using VendasApi.Data;
using VendasApi.Model;

namespace VendasApi.Repositories
{
    public class VendedorRepository
    {
        private readonly AppDbContext _context;

        public VendedorRepository(AppDbContext context)
        {
            _context = context;
        }

        public Vendedor GetById(long id)
        {
            return _context.Vendedores.Find(id);
        }
    }
}
