﻿using Microsoft.EntityFrameworkCore;
using VendasApi.Data;
using VendasApi.Model;

namespace VendasApi.Repositories
{
    public class VendaRepository
    {
        private readonly AppDbContext _context;

        public VendaRepository(AppDbContext context)
        {
            _context = context;
        }

        public Venda Add(Venda venda)
        {
            _context.Vendas.Add(venda);
            _context.SaveChanges();
            return venda;
        }

        public Venda GetById(long id)
        {
            return _context.Vendas
                .Include(v => v.Vendedor)
                .Include(v => v.Itens)
                .FirstOrDefault(v => v.Id == id);
        }

        public void Update(Venda venda)
        {
            _context.Vendas.Update(venda);
            _context.SaveChanges();
        }
    }
}
