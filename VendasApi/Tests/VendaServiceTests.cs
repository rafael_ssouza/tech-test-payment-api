﻿using Microsoft.EntityFrameworkCore;
using VendasApi.Data;
using VendasApi.DTO;
using VendasApi.Model;
using VendasApi.Repositories;
using VendasApi.Services;
using Xunit;

namespace VendasApi.Tests
{
    public class VendaServiceTests
    {
        private readonly VendaService _service;
        private readonly AppDbContext _context;
        private readonly VendaRepository _vendaRepository;
        private readonly VendedorRepository _vendedorRepository;

        public VendaServiceTests()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: "TestDb")
                .Options;
            _context = new AppDbContext(options);
            _vendaRepository = new VendaRepository(_context);
            _vendedorRepository = new VendedorRepository(_context);
            _service = new VendaService(_vendaRepository, _vendedorRepository);
        }

        [Fact]
        public void RegistrarVenda_DeveRegistrarVendaComStatusAguardandoPagamento()
        {
            var vendedor = new Vendedor { Cpf = "12345678900", Nome = "Teste", Email = "teste@teste.com", Telefone = "123456789" };
            _context.Vendedores.Add(vendedor);
            _context.SaveChanges();

            var vendaDTO = new VendaDTO
            {
                Vendedor = new VendedorDTO { Id = vendedor.Id },
                Itens = new List<ItemDTO> { new ItemDTO { Nome = "Item 1", Preco = 10.0 } }
            };

            var venda = _service.RegistrarVenda(vendaDTO);

            Assert.NotNull(venda);
            Assert.Equal("Aguardando pagamento", venda.Status);
        }

        [Fact]
        public void AtualizarStatus_DeveAtualizarStatusCorretamente()
        {
            var vendedor = new Vendedor { Cpf = "12345678900", Nome = "Teste", Email = "teste@teste.com", Telefone = "123456789" };
            _context.Vendedores.Add(vendedor);
            _context.SaveChanges();

            var vendaDTO = new VendaDTO
            {
                Vendedor = new VendedorDTO { Id = vendedor.Id },
                Itens = new List<ItemDTO> { new ItemDTO { Nome = "Item 1", Preco = 10.0 } }
            };

            var venda = _service.RegistrarVenda(vendaDTO);

            venda = _service.AtualizarStatus(venda.Id, "Pagamento aprovado");

            Assert.Equal("Pagamento aprovado", venda.Status);
        }

        [Fact]
        public void AtualizarStatus_DeveLancarExcecaoParaTransicaoInvalida()
        {
            var vendedor = new Vendedor { Cpf = "12345678900", Nome = "Teste", Email = "teste@teste.com", Telefone = "123456789" };
            _context.Vendedores.Add(vendedor);
            _context.SaveChanges();

            var vendaDTO = new VendaDTO
            {
                Vendedor = new VendedorDTO { Id = vendedor.Id },
                Itens = new List<ItemDTO> { new ItemDTO { Nome = "Item 1", Preco = 10.0 } }
            };

            var venda = _service.RegistrarVenda(vendaDTO);

            Assert.Throws<ArgumentException>(() => _service.AtualizarStatus(venda.Id, "Entregue"));
        }
    }
}
