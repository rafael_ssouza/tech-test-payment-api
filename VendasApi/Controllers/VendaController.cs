﻿using Microsoft.AspNetCore.Mvc;
using VendasApi.DTO;
using VendasApi.Model;
using VendasApi.Services;

namespace VendasApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaService _vendaService;

        public VendaController(VendaService vendaService)
        {
            _vendaService = vendaService;
        }

        [HttpPost]
        public ActionResult<Venda> RegistrarVenda([FromBody] VendaDTO vendaDTO)
        {
            var venda = _vendaService.RegistrarVenda(vendaDTO);
            return CreatedAtAction(nameof(BuscarVenda), new { id = venda.Id }, venda);
        }

        [HttpGet("{id}")]
        public ActionResult<Venda> BuscarVenda(long id)
        {
            var venda = _vendaService.BuscarVenda(id);
            if (venda == null)
                return NotFound();

            return venda;
        }

        [HttpPut("{id}/status")]
        public ActionResult<Venda> AtualizarStatus(long id, [FromQuery] string status)
        {
            try
            {
                var venda = _vendaService.AtualizarStatus(id, status);
                return venda;
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
