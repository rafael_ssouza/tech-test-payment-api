﻿namespace VendasApi.Model
{
    public class Venda
    {
        public long Id { get; set; }
        public Vendedor Vendedor { get; set; }
        public List<Item> Itens { get; set; }
        public DateTime Data { get; set; }
        public string Status { get; set; }
    }
}
