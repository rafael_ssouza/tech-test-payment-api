﻿namespace VendasApi.Model
{
    public class Item
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public double Preco { get; set; }
    }
}
